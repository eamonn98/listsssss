Terraria pre-hardmode
=============================

Bosses
---------------------------
### 1. ~~Eye of Cthulhu~~

![eye of cthulhu](https://c-4tvylwolbz88x24nhtlwlkphx2ejbyzljkux2ejvt.g00.gamepedia.com/g00/3_c-4alyyhyph.nhtlwlkph.jvt_/c-4TVYLWOLBZ88x24oaawzx3ax2fx2fnhtlwlkph.jbyzljku.jvtx2falyyhyph_nhtlwlkphx2fix2fi3x2fHupthalk_Lfl_vm_Jaobsob.npmx3fclyzpvux3d5im5h8212243m53443595l012800ljj6x26p87j.thyrx3dpthnl_$/$/$/$/$ "Eye of Cthulhu")

Spawns after using Suspicious Looking Eye item.

Demonite ore farm


### 2. King Slime

![king slime](https://c-4tvylwolbz88x24nhtlwlkphx2ejbyzljkux2ejvt.g00.gamepedia.com/g00/3_c-4alyyhyph.nhtlwlkph.jvt_/c-4TVYLWOLBZ88x24oaawzx3ax2fx2fnhtlwlkph.jbyzljku.jvtx2falyyhyph_nhtlwlkphx2f9x2f9mx2fHupthalk_Rpun_Zsptl.npmx3fclyzpvux3di358806mm933717k3k75mhmji61i9723x26p87j.thyrx3dpthnl_$/$/$/$/$ "King Slime")

Spawns after killing 150 slimes during slime rainfall


### 3. ~~Skeletron~~

![Skeletron](https://c-4tvylwolbz88x24nhtlwlkphx2ejbyzljkux2ejvt.g00.gamepedia.com/g00/3_c-4alyyhyph.nhtlwlkph.jvt_/c-4TVYLWOLBZ88x24oaawzx3ax2fx2fnhtlwlkph.jbyzljku.jvtx2falyyhyph_nhtlwlkphx2faobtix2flx2flhx2fZrlslayvu.wunx2f922we-Zrlslayvu.wunx3fclyzpvux3d21m9091k159843m9mm869j332mii3h97x26p87j.thyrx3dpthnl_$/$/$/$/$/$/$ "Skeletron")

Spawns after speaking to Old Man NPC at dungeon entrance, or after killing Clothier NPC using Clothier Voodoo Doll at night

Drops Book of Skulls - maginc weapon probaby worth farming


### 4. Eater of Worlds

![Eater of Worlds](https://c-4tvylwolbz88x24nhtlwlkphx2ejbyzljkux2ejvt.g00.gamepedia.com/g00/3_c-4alyyhyph.nhtlwlkph.jvt_/c-4TVYLWOLBZ88x24oaawzx3ax2fx2fnhtlwlkph.jbyzljku.jvtx2falyyhyph_nhtlwlkphx2faobtix2f9x2f9kx2fLhaly_vm_Dvyskz.wunx2f922we-Lhaly_vm_Dvyskz.wunx3fclyzpvux3d862l3k16m3hl229h6kilkl3k98k7lhi2x26p87j.thyrx3dpthnl_$/$/$/$/$/$/$ "Eater of Worlds")


### 5. Brain of Cthulhu (N/A)

![Brain of Cthulhu](https://c-4tvylwolbz88x24nhtlwlkphx2ejbyzljkux2ejvt.g00.gamepedia.com/g00/3_c-4alyyhyph.nhtlwlkph.jvt_/c-4TVYLWOLBZ88x24oaawzx3ax2fx2fnhtlwlkph.jbyzljku.jvtx2falyyhyph_nhtlwlkphx2fjx2fjhx2fHupthalk_Iyhpu_vm_Jaobsob.npmx3fclyzpvux3dl32li040kih84lj7k8m661lji943kk02x26p87j.thyrx3dpthnl_$/$/$/$/$ "Brain of Cthulhu")

Spawns after using Bloody Spine item

Drops Tissue Samples

Only spawns in worlds with Crimson biomes. Our world contains Corruption


### 6. ~~Queen Bee~~

![Queen Bee](https://c-4tvylwolbz88x24nhtlwlkphx2ejbyzljkux2ejvt.g00.gamepedia.com/g00/3_c-4alyyhyph.nhtlwlkph.jvt_/c-4TVYLWOLBZ88x24oaawzx3ax2fx2fnhtlwlkph.jbyzljku.jvtx2falyyhyph_nhtlwlkphx2fmx2fm2x2fHupthalk_Xbllu_Ill.npmx3fclyzpvux3dk5k13h500ill77962il9726lhim127i3x26p87j.thyrx3dpthnl_$/$/$/$/$ "Queen Bee")

Spawns by destroying larva within bee hives in the Underground Jungle.

Drops Bee Cannon - useful against Wall of Flesh boss
Drops Honeyed Goggles, used to summon Bee Mount, a limited time flying mount


### 7. Wall of Flesh

![Wall of Flesh](https://c-4tvylwolbz88x24nhtlwlkphx2ejbyzljkux2ejvt.g00.gamepedia.com/g00/3_c-4alyyhyph.nhtlwlkph.jvt_/c-4TVYLWOLBZ88x24oaawzx3ax2fx2fnhtlwlkph.jbyzljku.jvtx2falyyhyph_nhtlwlkphx2faobtix2f9x2f97x2fHupthalk_Dhss_vm_Mslzo.npmx2f827we-Hupthalk_Dhss_vm_Mslzo.npmx3fclyzpvux3dk63613mj184940147ih91h3l36j510ljx26p87j.thyrx3dpthnl_$/$/$/$/$/$/$ "Wall of Flesh")

Spawns after throwing Guide Voodoo Doll into a pool or lava

Drops Breaker Blade, Elbmens, Laser Rifle. Pwnhammer

Defeating WoF causes the world to transition into Hardmode



Item build targets
---------------------------
### Jellyfish Diving Gear
* Diving Gear
	- Flipper; *Water Chests, which are found submerged in water*
	- Diving Helmet; *2% (1/50) chance of being dropped by Sharks and Orcas*
* Jellyfish Necklace; *1% (1/100) chance to drop from Pink, Blue, or Green Jellyfish in the Ocean and Underground layer*

### PDA
* GPS
    - Gold/Platinum Watch; *10 Bars + Chain @ Table+Chair crafting station*
    - Depth Meter; *dropped by Cave Bats, Giant Bats, Jungle Bats, and Ice Bats, with a 1 in 100 chance (1%) *
    - Compass; *2% (1 in 50) chance to be dropped by Salamanders, Giant Shellys, Crawdads, Mother Slimes, Piranhas, Snow Flinxes, Undead Vikings, and Armored Vikings*
* Fish Finder
    - Fisherman's Pocket Guide; *2.5% (1 in 40) chance reward for completing Fishing quests for the Angler NPC*
    - Weather Radio; *2.5% (1 in 40) chance reward for completing Fishing quests*
    - Sextant;  *2.5% (1 in 40) chance reward for completing Fishing quests for the Angler NPC*
* Goblin Tech
    - Metal Detector; *Dropped by Nymph in Cavern layer with 50% chance*
    - Stopwatch; *Sold by Traveling Merchant*
    - DPS Meter; *Sold by Traveling Merchant*
* REK 3000
    - Radar; *from looting Wooden Chests found on or near the surface or from Wooden Crates*
    - Lifeform Analyzer; *purchased from the Traveling Merchant for 5 Gold*
    - Tally Counter; *1/100 (1%) chance of being dropped from Angry Bones, Cursed Skulls, and Dark Casters, all of which are found in the Dungeon*

### Angler Tackle Bag
* High Test Fishing Line; *randomly awarded by the Angler for completing a quest*
* Tackle Box; *randomly awarded by the Angler for completing a quest*
* Angler Earring; *randomly awarded by the Angler for completing a quest*

### Night's Edge
* Light's Bane; *Crafted using 10 Demonite Bars*
* Muramasa; *Obtained from gold chests within the Dungeon*
* Blade of Grass; *Crafted using 12 Jungle Spores and 12 Stingers*
* Fiery Greatsword; *Crafted with 20 Hellstone Bars*

### Star Cannon
* Minishark; *Bought from Arms Dealer for 35 gold*
* Meteorite Bar (20)
* Fallin Star (5)


Items worth acquiring
---------------------------
* Golden Bug Net
* Bucket
* Sunfury
* Cascade
* Bee Cannon
* Demon Scythe
* Book of Skulls
* Flower of Fire
* Flamelash
* Space Gun
* Imp Staff
* Rockfish
* Drax